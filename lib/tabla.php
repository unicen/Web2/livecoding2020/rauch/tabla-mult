<?php

function showTabla($limite) {

    echo '
    <!DOCTYPE html>
        <html lang="en">
        <head>
            <base href="'.BASE_URL.'">
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Tabla de Multiplicar '.$limite.'</title>
            <link rel="stylesheet" href="css/style.css">
        </head>
        <body>
    ';

    echo "<nav>";
        echo "<a href=''><h3>volver al home</h3></a>";
    echo "<nav>";
    

    //Imprime la tabla de multiplcar
    echo "<table>";
    for ($fila=0; $fila<=$limite; $fila++) {
        echo "<tr>";
        for ($col=0; $col<=$limite; $col++) {
            if ($col == 0)
                echo "<td class='resaltado'>" . $fila . "</td>";
            elseif ($fila == 0)
               echo "<td class='resaltado'>" . $col . "</td>";
            elseif ($fila == $col)
                echo "<td class='resaltado'>" . ($fila*$col) . "</td>";
            else
                echo "<td>" . ($fila*$col) . "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";

    echo '
        </body>
    </html>
    ';
}


function showMenu() {
    echo '
    <!DOCTYPE html>
        <html lang="en">
        <head>
            <base href="http://localhost/web2/tp1/ej5/">
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Tabla de Multiplicar</title>
            <link rel="stylesheet" href="css/style.css">
        </head>
        <body>
    ';

    $random = random_int(1, 100);
    echo "<nav>";
        echo "<span class='logo resaltado'>LAS TABLAS</span>";
        echo "<a href='tabla/{$random}'>tabla al azar</a>";
        echo "<a href='about'>About</a>";
    echo "<nav>";

    echo "<h1>Seleccione una tabla</h1>";

    echo "<ul class='tablas'>";
    for ($i=5; $i<=100; $i+=5) {
        echo "<li><a href='tabla/{$i}'>tabla del {$i} </a></li>";
        //echo "<a href=''>tabla del " . $i . "</a>";
    }
    echo "</nav>";

    echo '
        </body>
    </html>
    ';
}

function showAbout() {
    echo "Hola! ;)";
}

function show404() {
    echo "<h1> 404 </h1>";
    echo "<h2>Página no encontrada </h2>";
}

?>