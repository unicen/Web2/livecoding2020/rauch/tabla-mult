<?php
    require_once 'lib/tabla.php';

    // definimos la base url de forma dinamica
    define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

    // define una acción por defecto
    if (empty($_GET['action'])) {
        $_GET['action'] = 'menu';
    } 

    // toma la acción que viene del usuario y parsea los parámetros
    $accion = $_GET['action']; 
    $parametros = explode('/', $accion);
    //var_dump($parametros); die; // like console.log();

    // decide que camino tomar según TABLA DE RUTEO
    switch ($parametros[0]) {
        case 'tabla':
            if (isset($parametros[1]))
                showTabla($parametros[1]);
            else
                echo "Se debe ingresar un límite";
        break;

        case 'menu':
            showMenu();
        break;

        case 'about':
            showAbout();
        break;
    
        default: 
            show404();
        break;
    }

?>
